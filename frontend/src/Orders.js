import React, { Component } from 'react'
import { Route, Link } from 'react-router-dom'

import { Content } from './components/layout'

class Row extends Component {
  render () {
    const { url, order } = this.props
    return (
      <li key={order.id}>
        <Link to={`${url}/${order.id}`}>Order ${order.id}</Link>
      </li>
    )
  }
}

const Table = ({ url, orders }) => {
  if (orders.length > 0) {
    return ([
      <p key='0'>Orders</p>,
      <ul key='1'>
        {orders.map(order => (
          <Row order={order} url={url} />
        ))}
      </ul>
    ])
  } else {
    return (<ul />)
  }
}

class Order extends Component {
  constructor (props) {
    super(props)

    this.state = {
      orderId: props.match.params.orderId
    }
  }

  render () {
    return ([
      <p key='0'>Order #{this.state.orderId}</p>,
      <div key='1'>Info</div>
    ])
  }
}

class Orders extends Component {
  constructor () {
    super()

    this.state = {
      orders: []
    }
  }

  componentDidMount () {
    fetch('http://localhost:8080/orderManager/GetOrders')
      .then(res => res.json())
      .then(json => {
        const { orders } = json
        if (orders.length > 0) {
          this.setState({ orders: orders })
        }
      })
      .catch(err => console.log(err))
  }

  render () {
    const { url } = this.props.match
    const { orders } = this.state
    return (
      <Content>
        <Route
          exact
          path={url}
          render={() => <Table orders={orders} url={url} />}
        />
        <Route
          path={`${url}/:orderId`}
          component={Order} />
      </Content>
    )
  }
}

export default Orders

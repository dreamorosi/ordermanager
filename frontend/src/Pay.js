import React, { Component } from 'react'

import { Content } from './components/layout'
import Payment from './components/pay/'
import Shipping from './components/ship/'
import Checkout from './components/checkout/'

class Pay extends Component {
  constructor () {
    super()
    this.state = {
      payment: {},
      customer: {
        returning: false
      },
      pages: 3,
      current: 0,
      checkoutStep: 0
    }

    this.setPayment = this.setPayment.bind(this)
    this.setCustomer = this.setCustomer.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.prev = this.prev.bind(this)
    this.next = this.next.bind(this)
    this.renderPage = this.renderPage.bind(this)
  }

  setPayment (payment) {
    payment['currency'] = 'USD'
    this.setState({
      payment: payment
    })

    this.next()
  }

  setCustomer (customer, next = false) {
    this.setState({
      customer: customer
    })

    if (next) {
      this.next()
    }
  }

  handleSubmit () {
    this.setState({
      checkoutStep: 1
    })
    console.log('Should submit data to API')
  }

  prev () {
    let { current } = this.state
    if (current === 0) {
      return
    }

    current--
    this.setState({
      current: current
    })
  }

  next () {
    let { current, pages } = this.state
    if (current === pages - 1) {
      return
    }

    current++
    this.setState({
      current: current
    })
  }

  renderPage () {
    switch (this.state.current) {
      case 0:
        return <Payment
          paymentCallback={this.setPayment}
          customerCallback={this.setCustomer} />
      case 1:
        return <Shipping
          customerCallback={this.setCustomer}
          back={this.prev} />
      default:
        return <Checkout
          customer={this.state.customer}
          payment={this.state.payment}
          submit={this.handleSubmit}
          back={this.prev}
          step={this.state.checkoutStep} />
    }
  }

  render () {
    return (
      <Content>
        {this.renderPage()}
      </Content>
    )
  }
}

export default Pay

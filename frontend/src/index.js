import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

import { Wrapper, Header } from './components/layout'
import Pay from './Pay'
import Orders from './Orders'

const App = () => (
  <Router>
    <Wrapper>
      <Header>
        <Link to='./'>Logo</Link>
        <Link to='./pay'>Pay</Link>
        <Link to='./orders'>My orders</Link>
        <Link to='#'>Logout</Link>
      </Header>

      <Route exact path='/' component={Pay} />
      <Route path='/pay' component={Pay} />
      <Route path='/orders' component={Orders} />
    </Wrapper>
  </Router>
)

ReactDOM.render(<App />, document.getElementById('root'))

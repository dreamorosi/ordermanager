import styled from 'styled-components'
import { media } from './style-utils'

const headerHeight = 60

const colors = {
  'blackish': '#333',
  'white': '#FFF'
}

export const Wrapper = styled.section`
  width: 100vw;
  min-height: 100vh;
`

export const Header = styled.header`
  width: 100vw;
  height: ${headerHeight}px;
  display: flex;
  align-items: center;
  padding: 0 50px;
  ${media.handheld`
    padding: 0 5px;
  `}
  & a {
    color: ${colors.blackish};
    margin: 0 0 0 10px;
    ${media.handheld`
      margin: 0 0 0 20px;
    `}
  }
  & a:first-child {
    margin: 0 auto 0 0;
    font-size: 2em;
    font-weight: bold;
  }
  & a:last-child {
    margin: 0 0 0 45px;
    ${media.handheld`
      margin: 0 0 0 40px;
    `}
  }
`

export const Content = styled.section`
  width: 100vw;
  min-height: calc(100vh - ${headerHeight}px);
  color: ${colors.blackish};
  padding: 25px 50px;
  ${media.handheld`
    padding: 0 5px;
  `}
`

// Page.withComponent('form').extend`

export const Page = styled.div`
  width: 600px;
  height: 300px;
  margin: 100px auto;
  padding: 15px 10px;
  ${media.handheld`
    margin: 15px 0;
    padding: 0;
  `}
  box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)
`

export const Form = styled.form`
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`

export const Input = styled.div`
  width: ${props => props.col === '1' ? '100%' : '50%'}
  display: flex;
  flex-direction: column;
  padding: 0 15px 10px;
  & label {
    text-transform: uppercase;
    font-size: .75em;
    margin: 0;
  }
`

export const CheckoutWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  padding: 15px;
`

export const CheckoutCol = styled.div`
  width: 50%;
  & p {
    text-transform: uppercase;
    font-size: .75em;
    margin: 20px 0 0 0;
  }
  & p:first-of-type {
    margin: 0;
  }
  & span {
    display: block;
  }
  & h2 {
    text-align: center;
    margin-bottom: 50px;
  }
  &:nth-child(2) {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`

export const ThankYouWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Btn = styled.button`
  border: 1px solid ${colors.blackish};
  background: ${colors.white}
`

export const BackBtn = styled.span`
  color: ${colors.blackish};
  cursor: pointer;
`

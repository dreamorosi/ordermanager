import React, { Component } from 'react'
import styled from 'styled-components'

import { CheckoutCol, Btn, BackBtn } from '../layout'

const Back = styled(BackBtn)`
  margin: 10px auto;
`

class CheckoutBox extends Component {
  render () {
    const { customer, payment, submit, back } = this.props
    let card = payment['cc-number']
    card = card.substr(card.length - 4, card.length - 1)
    return (
      [
        <CheckoutCol key={0}>
          <p>Customer Info</p>
          <span>{customer.fullName}</span>
          <span>{customer['address-line1']}</span>
          <span>{customer['postal-code']}</span>
          <span>{customer.country}</span>
          <p>Email Address</p>
          {payment.email}
          <p>Card Ending with</p>
          {'****' + card}
        </CheckoutCol>,
        <CheckoutCol key={1}>
          <h2>{payment.currency + ' ' + payment.amount}</h2>
          <Btn onClick={submit}>Pay Now</Btn>
          <Back onClick={back}>Cancel</Back>
        </CheckoutCol>
      ]
    )
  }
}

export default CheckoutBox

import React, { Component } from 'react'

import { ThankYouWrapper } from '../layout'

class ThankYouBox extends Component {
  render () {
    return (
      <ThankYouWrapper>
        <h2>Thank You</h2>
      </ThankYouWrapper>
    )
  }
}

export default ThankYouBox

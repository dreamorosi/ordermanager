import React, { Component } from 'react'

import { Page, CheckoutWrapper } from '../layout'
import CheckoutBox from './CheckoutBox'
import ThankYouBox from './ThankYouBox'

class Checkout extends Component {
  renderStep (that) {
    if (!that.props.step) {
      return <CheckoutBox
        submit={that.handleCheckout}
        {...that.props} />
    } else {
      return <ThankYouBox />
    }
  }

  render () {
    return (
      <Page>
        {this.props.children}
        <CheckoutWrapper>
          {this.renderStep(this)}
        </CheckoutWrapper>
      </Page>
    )
  }
}

export default Checkout

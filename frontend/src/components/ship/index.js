import React, { Component } from 'react'

import { Page } from '../layout'
import ShippingForm from './ShippingForm'

class Shipping extends Component {
  render () {
    const { children, customerCallback, back } = this.props
    return (
      <Page>
        {children}
        <ShippingForm
          customerCallback={customerCallback}
          back={back} />
      </Page>
    )
  }
}

export default Shipping

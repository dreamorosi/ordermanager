import React, { Component } from 'react'
import styled from 'styled-components'

import { Form, Input, Btn, BackBtn } from '../layout'

const Back = styled(BackBtn)`
  margin: auto 20px auto 0;
`

class PaymentForm extends Component {
  constructor (props) {
    super()
  }

  handleSubmitShipping (e) {
    e.preventDefault()
    const formData = {}
    for (const field in this.refs) {
      formData[field] = this.refs[field].value
    }

    this.props.customerCallback(formData, true)
  }

  render () {
    return (
      <Form action='#' method='post' onSubmit={(e) => this.handleSubmitShipping(e)}>
        <Input col='1'>
          <label>Full Name</label>
          <input
            ref='fullName'
            name='fullName'
            type='text'
            tabIndex='1'
            autoComplete='name'
            defaultValue="Andrea Amorosi"
            required />
        </Input>
        <hr />
        <Input col='1'>
          <label>Address</label>
          <input
            ref='address-line1'
            name='address-line1'
            type='text'
            tabIndex='2'
            autoComplete='address-line1'
            defaultValue="Baker Street 114"
            required />
        </Input>
        <Input col='2'>
          <label>Zip</label>
          <input
            ref='postal-code'
            name='postal-code'
            type='text'
            tabIndex='3'
            autoComplete='postal-code'
            defaultValue="RG6 6BQ"
            required />
        </Input>
        <Input col='2'>
          <label>Country</label>
          <input
            ref='country'
            name='country'
            type='text'
            tabIndex='4'
            autoComplete='country'
            defaultValue="United Kingdom"
            required />
        </Input>
        <Back onClick={() => this.props.back()}>Back</Back>
        <Btn type='submit'>Checkout</Btn>
      </Form>
    )
  }
}

export default PaymentForm

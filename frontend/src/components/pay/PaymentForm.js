import React, { Component } from 'react'
import debounce from 'lodash.debounce'

import { Form, Input, Btn } from '../layout'

class PaymentForm extends Component {
  handleSubmitPayment (e) {
    e.preventDefault()
    const formData = {}
    for (const field in this.refs) {
      formData[field] = this.refs[field].value
    }

    this.props.paymentCallback(formData)
  }

  componentWillMount () {
    this.handleEmailChange = debounce(e => {
      if (!e.target.validity.valid) {
        return
      }

      // let value = e.target.value
      console.log('Should lookup email here')
      this.props.customerCallback({
        returning: true,
        fullName: 'Andrea Amorosi'
      })
    }, 500)
  }

  handleEmailChangeDebounced (e) {
    e.persist()
    this.handleEmailChange(e)
  }

  render () {
    return (
      <Form
        action='#'
        method='post'
        onSubmit={(e) => this.handleSubmitPayment(e)}>
        <Input col='1'>
          <label>Your Email Address</label>
          <input
            ref='email'
            name='email'
            type='email'
            tabIndex='1'
            autoComplete='email'
            onChange={(e) => this.handleEmailChangeDebounced(e)}
            defaultValue="dreamorosi@gmail.com"
            required />
        </Input>
        <hr />
        <Input col='1'>
          <label>Credit Card</label>
          <input
            ref='cc-number'
            name='cc-number'
            type='number'
            tabIndex='2'
            autoComplete='cc-number'
            defaultValue="4242424242424242"
            required />
        </Input>
        <Input col='2'>
          <label>Exp date</label>
          <input
            ref='cc-exp'
            name='cc-exp'
            type='text'
            tabIndex='3'
            autoComplete='cc-exp'
            defaultValue="12/18"
            required />
        </Input>
        <Input col='2'>
          <label>Security Code</label>
          <input
            ref='cc-csc'
            name='cc-csc'
            type='number'
            tabIndex='4'
            autoComplete='cc-csc'
            defaultValue="123"
            required />
        </Input>
        <Input col='1'>
          <label>Amount</label>
          <input
            ref='amount'
            name='amount'
            type='number'
            tabIndex='5'
            autoComplete='off'
            defaultValue="120"
            required />
        </Input>
        <Btn type='submit'>Pay</Btn>
      </Form>
    )
  }
}

export default PaymentForm

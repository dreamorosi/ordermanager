import React, { Component } from 'react'

import { Page } from '../layout'
import PaymentForm from './PaymentForm'

class Payment extends Component {
  render () {
    return (
      <Page>
        {this.props.children}
        <PaymentForm
          paymentCallback={this.props.paymentCallback}
          customerCallback={this.props.customerCallback} />
      </Page>
    )
  }
}

export default Payment

package com.ordermanager.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.ordermanager.java.model.Order;

/**
 * Servlet implementation class UpdateOrder
 */
@WebServlet("/UpdateOrder")
public class UpdateOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateOrder() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//permissions
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		//parameters
		String id = request.getParameter("uuid");
		String amount = request.getParameter("amount");
		String currency = request.getParameter("currency");
		String notes = request.getParameter("notes");
		String customer_id = request.getParameter("customer");
		
		Order o = new Order();
		
		boolean issaved = o.updateOrder(id, amount, currency, notes, customer_id);
		
		JSONArray list = new JSONArray();
		JSONObject item = new JSONObject();
		
		
		if (issaved) {
			item.put("result", true);
		}else {
			item.put("result", false);
		}
		list.add(item);
		
		
		response.getWriter().append(list.toJSONString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package com.ordermanager.java.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

public class Order {
	// schema
	private String id;
	private int number;
	private float amount;
	private String currency;
	private String notes;
	private String customer_id;
	
	// db conn
	private Connection conn = null;
	private Statement stmt = null;
	
	public Order() {
		try {
			// register driver
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//connection
			this.conn = DriverManager.getConnection("jdbc:mysql://51.254.211.97/fum?user=andre&password=pazzword");
			
			//create statement
			this.stmt = this.conn.createStatement();
		} catch(Exception ex) {
			System.out.println("error on Order initialization: " + ex.getMessage());
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public boolean createOrder(String amount, String currency, String notes, String customer_id) {
		try {
			UUID uuid = UUID.randomUUID();
			//SQL String
			String fields = "(id, amount, currency";
			String values = "VALUES ('" + uuid + "', '" + amount + "', '" + currency +"'";
			if (notes != null) {
				fields += ", notes";
				values += ", '" + notes + "'";
			}
			
			if (customer_id != null) {
				fields += ", customer_id";
				values += ", '" + customer_id + "'";
			}
			
			String sql = "INSERT INTO orders " + fields + ") " + values + ")";
			//Print SQL String
			System.out.println(sql);
			//Execute query
			this.stmt.executeUpdate(sql);
			

			return true;
		}catch (Exception ex){
			System.out.println("error on write order" + ex.getMessage());
			return false;
		}
	}
	
	public ArrayList<Order> getOrders() {
		try {
			//SQL String
			String sql = "SELECT * FROM orders";
			//Print SQL String		
			System.out.println(sql);
			
			//Execute query
			ResultSet rs = this.stmt.executeQuery(sql);
			
			ArrayList<Order> orders = new ArrayList<Order>();
			
			while (rs.next()) {
				Order order= new Order();
				order.setId(rs.getString("id"));
				order.setNumber(rs.getInt("number"));
				order.setAmount(rs.getFloat("amount"));
				order.setCurrency(rs.getString("currency"));
				order.setNotes(rs.getString("notes"));
				order.setCustomer_id(rs.getString("customer_id"));

				orders.add(order);
			}

			return orders;
		}catch (Exception ex){
			System.out.println("error on get orders " + ex.getMessage());
			return null;
		}		
	}

	public boolean updateOrder(String id, String amount, String currency, String notes, String customer_id) {
		try {
			//SQL String
			ArrayList<String> sqlFrags = new ArrayList<String>();
			if (amount != null) {
				sqlFrags.add("amount = '" + amount + "'");
			}

			if (currency != null) {
				sqlFrags.add("currency = '" + currency + "'");
			}
			
			if (notes != null) {
				sqlFrags.add("notes = '" + notes + "'");
			}
			
			if (customer_id != null) {
				sqlFrags.add("customer_id = '" + customer_id + "'");
			}
			
			String binds = String.join(",", sqlFrags);
			
			String sql = "UPDATE orders SET " + binds + " WHERE id = '" + id + "'";
			//Print SQL String
			System.out.println(sql);
			//Execute query
			int rowAffected = this.stmt.executeUpdate(sql);

			if (rowAffected > 0) {
				return true;
			} else {
				throw new Exception("Error deleting data");
			}
		}catch (Exception ex){
			System.out.println("error on update order" + ex.getMessage());
			return false;
		}
	}

	public boolean deleteOrder(String id) {
		try {
			String sql = "DELETE FROM orders WHERE id = '" + id + "'";
			
			//Print SQL String
			System.out.println(sql);
			//Execute query
			int rowAffected = this.stmt.executeUpdate(sql);
			
			if (rowAffected > 0) {
				return true;
			} else {
				throw new Exception("Error deleting data");
			}
		} catch (Exception ex){
			System.out.println("error on delete order " + ex.getMessage());
			return false;
		}
	}
}

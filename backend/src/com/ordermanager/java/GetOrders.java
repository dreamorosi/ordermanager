package com.ordermanager.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.ordermanager.java.model.Order;

/**
 * Servlet implementation class GetOrders
 */
@WebServlet("/GetOrders")
public class GetOrders extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetOrders() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//permissions
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		try {
						
			//Model
			Order o = new Order();
			ArrayList<Order> orders = o.getOrders();
			

			//Add Items
			JSONArray list = new JSONArray();
			for (Order Order: orders) {
				
				JSONObject item = new JSONObject();
				item.put("id", Order.getId());
				item.put("number", Order.getNumber());
				item.put("amount", Order.getAmount());
				item.put("currency", Order.getCurrency());
				item.put("notes", Order.getNotes());
				item.put("customerID", Order.getCustomer_id());
				
				list.add(item);  
			}
			
			//main root
			JSONObject mainRoot = new JSONObject();
			mainRoot.put("orders",list);
			
			
			response.getWriter().append(mainRoot.toJSONString());
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
